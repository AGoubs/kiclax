const express = require('express');
const mongodb = require('mongodb');

const router = express.Router();

//Get section 

 router.get('/', async (req, res) => {
     const section = await loadSectionCollection();
     res.send(await section.find({}).toArray());
});


// Add Posts

router.post('/', async(req,res)  => {
    const section = await loadSectionCollection();
    await section.insertOne({
        text: req.body.text,
        createdAt: new Date()
    })   
});


async function loadSectionCollection(){
    const client = await mongodb.MongoClient.connect
    ('mongodb+srv://antoine:Naobito12@dbkiclax-tbhqu.mongodb.net/test?retryWrites=true&w=majority', {
        useNewUrlParser: true 
    });

    return client.db('dbkiclax').collection('section');
}
module.exports = router;