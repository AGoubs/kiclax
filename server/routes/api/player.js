const express = require('express');
const mongodb = require('mongodb');

const router = express.Router();

//Get section 

 router.get('/', async (req, res) => {
     const player = await loadPlayerCollection();
     res.send(await player.find({}).toArray());
});


// Add Posts

router.post('/', async(req,res)  => {
    const section = await loadPlayerCollection();
    await section.insertOne({
        text: req.body.text,
        point:req.body.point,
        createdAt: new Date()
    })   
});


async function loadPlayerCollection(){
    const client = await mongodb.MongoClient.connect
    ('mongodb+srv://antoine:Naobito12@dbkiclax-tbhqu.mongodb.net/test?retryWrites=true&w=majority', {
        useNewUrlParser: true 
    });

    return client.db('dbkiclax').collection('player');
}
module.exports = router;