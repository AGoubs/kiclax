const express = require('express');
const mongodb = require('mongodb');


const router = express.Router();

const listeward = require('./reward.json');


router.get('/', async (req, res) => {
    const reward = await loadRewardCollection();
    res.send(await reward.find({}).toArray());
});

router.post('/', async (req, res) => {
    const reward = await loadRewardCollection();
    await reward.insertOne({
        data: listeward.reward,
        createdAt: new Date(),
    });
});
 
async function loadRewardCollection(){
    const client = await mongodb.MongoClient.connect
    ('mongodb+srv://antoine:Naobito12@dbkiclax-tbhqu.mongodb.net/test?retryWrites=true&w=majority', {
        useNewUrlParser: true 
    });
    return client.db('dbkiclax').collection('reward');
}

module.exports = router;