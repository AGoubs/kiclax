const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const upload = require("express-fileupload");

const app = express();

app.use(upload());
app.use(bodyParser.json());
app.use(cors());

const posts = require('./routes/api/posts');
const section = require('./routes/api/section');
const player = require('./routes/api/player');
const reward = require('./routes/api/reward');


app.use('/api/section/',section);
app.use('/api/posts/',posts);
app.use('/api/player/',player);
app.use('/api/reward/',reward);

//route production

if(process.env.NODE_ENV === 'production'){

    //STATIC FOLDER
    app.use(express.static(__dirname + '/public/'));

    // SPA
    app.get('/*/', (req, res) => { res.sendFile(__dirname + '/public/index.html');
        
    });

}
const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`server de kiclax start on port ${port}`)); 