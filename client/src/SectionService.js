import axios from 'axios';

const url ="/api/section/";


    
class SectionService{


    static async getSection(){
        
        const res = await axios.get(url);
        return new Promise((resolve,reject) => {
            try {
                
                const data = res.data;
                resolve(
                    data.map(section => ({
                    ...section,
                    createdAt : new Date(section.createdAt)
                    }))
                );
            }catch(err){
                reject(err);
            }
        });
    }
    
    static insertSection(text){
        return axios.post(url,{
            text
        });
    }

}

export default SectionService;