import axios from 'axios';

const url ="/api/posts/";

class PostService {

    //Get Posts
    static async getPosts (){
        
        const res = await axios.get(url);
        return new Promise((resolve,reject) => {
            try {
               // const res = await axios.get(url);
                const data = res.data;
                resolve(
                    data.map(post => ({
                    ...post,
                    createdAt : new Date(post.createdAt)
                    }))
                );
            }catch(err){
                reject(err);
            }
        });
    }

    //Create Post

    static insertPost(pseudo,title,textarea,lien){
        return axios.post(url,{
            pseudo,
            title,
            textarea,
            lien
        });
    }

    //Delete Post

    static deletePost(id) {
        return axios.delete(`${url}${id}`);
    }

}


export default PostService;