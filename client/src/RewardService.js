import axios from 'axios';

const url ="/api/reward/";

class RewardService {

//Get Posts
static async getReward (){
        
    const res = await axios.get(url);
    return new Promise((resolve,reject) => {
        try {
           // const res = await axios.get(url);
            const data = res.data;
            resolve(
                data.map(reward => ({
                ...reward,
                createdAt : new Date(reward.createdAt)
                }))
            );
        }catch(err){
            reject(err);
        }
    });
}

//Create Post

static insertPost(){
    axios.post(url);
}
}

export default RewardService;