import axios from 'axios';

const url ="/api/player/";

class PlayerService {

    //Get Posts
    static async getPlayer (){
        
        const res = await axios.get(url);
        return new Promise((resolve,reject) => {
            try {
               
                const data = res.data;
                resolve(
                    data.map(player => ({
                    ...player,
                    createdAt : new Date(player.createdAt)
                    }))
                );
            }catch(err){
                reject(err);
            }
        });
    }

    //Create Post

    static insertPlayer(text,point){
        return axios.post(url,{
            text,
            point,
        });
    }


}

export default PlayerService;